// importing gulp
// gulp is a task runner
const gulp = require('gulp');
// importing gulp-sass
// gulp-sass is a gulp plugin to compile sass
const sass = require('gulp-sass');
// importing gulp-uglify
// gulp-uglify is a gulp plugin to minify javascript
const uglify = require('gulp-uglify');
// importing pump
// pump is a node library to control errors
const pump = require('pump');
// importing browser-sync
// browser-sync is a web server
const browserSync = require('browser-sync').create();

// task to display gulp running message on console
gulp.task('info-running', function(){
    console.log('Gulp está em execução! Para finalizar pressione CTRL+C');
});

// task to serve
gulp.task('server', function(){
    browserSync.init({
        server: {
            baseDir: "app/"
        }
    });
});

// task to reload server
gulp.task('reload', function(){
    browserSync.reload();
});

// task to minify javascript
gulp.task('compress-js', function(cb){
    pump([
        gulp.src(['./app/assets/js/*.js', './app/assets/js/library/*.js']),
        uglify(),
        gulp.dest('./app/assets/js-min'),
        browserSync.stream()
    ], cb);
});

// task to compile and minify sass
gulp.task('sass', function() {
    return gulp.src('./app/assets/stylesheets/global.scss')
    // compiling, minifying sass and display error logs
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./app/assets/stylesheets/_css'))
    .pipe(browserSync.stream());
});

// task to watch changes on css directory
gulp.task('watch-sass', function(){
    gulp.watch([
        './app/assets/stylesheets/global.scss',
        './app/assets/stylesheets/stylethemes/candy.scss',
        './app/components/**/*.scss'
    ], ['sass', 'info-running']);
});

// task to watch changes on index file
gulp.task('watch-html', function(){
    gulp.watch('app/**/*.html', ['reload', 'info-running']);
});

// task to watch js directory changes
gulp.task('watch-js', function(){
    gulp.watch([
        './app/assets/js/*.js',
        './app/assets/js/library/*.js'
    ], ['compress-js', 'info-running']);
});

// gulp default task
gulp.task('default', [
    'sass', 'compress-js',
    'server', 'info-running',
    'watch-sass', 'watch-js',
    'watch-html'
]);